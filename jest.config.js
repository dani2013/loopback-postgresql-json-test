'use strict';

module.exports = {
  verbose: true,
  rootDir: 'server',
  coverageDirectory: '<rootDir>/../coverage',
  testEnvironment: 'node',
  testRegex: '(/__tests__/.*|(\\.|/)(spec))\\.js$',
};
