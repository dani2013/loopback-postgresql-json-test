create table client (
    id serial not null primary key,
    firstName text not null,
    lastName text not null,
    props json
);

create table extended_client (
    id serial not null primary key,
    firstName text not null,
    lastName text not null,
    extendedAttr1 text,
    props json
);

create view all_clients(model, id, firstName, lastName, extendedAttr1, props) as
    select 'client', id, firstName, lastName, null, props from client
    union all
    select 'extended-client', id, firstName, lastName, extendedAttr1, props from extended_client;

insert into client (firstName, lastName, props) values ('homer', 'simpson', '{ "props1": "value1", "props2": "value2" }');
insert into extended_client (firstName, lastName, extendedAttr1, props) values ('bart', 'simpson', 'extended attribute', '{ "props3": "value3", "props4": "value4" }');
