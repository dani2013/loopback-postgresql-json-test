'use strict';

module.exports = {
  postgresql: {
    connector: 'postgresql',
    host: process.env.APP_DB_HOST,
    port: process.env.APP_DB_PORT,
    database: process.env.APP_DB_DATABASE,
    username: process.env.APP_DB_USERNAME,
    password: process.env.APP_DB_PASSWORD,
  },
};
