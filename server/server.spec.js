'use strict';

const http = require('http');
const superagent = require('superagent');

const app = require('./server');

describe('Server test', () => {
  let agent = null;
  let server = null;
  let baseUrl = null;

  beforeAll(done => {
    agent = superagent.agent();
    server = http.createServer(app);
    server.listen(0, () => {
      const port = server.address().port;
      baseUrl = `http://127.0.0.1:${port}`;
      done();
    });
  });

  afterAll(done => {
    server.close(() => {
      done();
    });
  });

  test('should be started', async () => {
    const response = await agent.get(`${baseUrl}/`);
    expect(response).toBeDefined();
    expect(response.status).toBe(200);
  });

  test('should return an empty array', async () => {
    const response = await agent.get(`${baseUrl}/api/clients`);
    expect(response).toBeDefined();
    expect(response.status).toBe(200);
    expect(response.body).toEqual([]);
  });

  test('should add a client', async () => {
    const response = await agent
      .post(`${baseUrl}/api/clients`)
      .send({ firstName: 'bart', lastName: 'simpson', props: { prop1: 'value1' } });
    expect(response).toBeDefined();
    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      firstName: 'bart',
      id: 1,
      lastName: 'simpson',
      props: { prop1: 'value1' },
    });
  });

  test('should get client with id 1', async () => {
    const response = await agent.get(`${baseUrl}/api/clients/1`);
    expect(response).toBeDefined();
    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      firstName: 'bart',
      id: 1,
      lastName: 'simpson',
      props: { prop1: 'value1' },
    });
  });
});
