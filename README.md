# LOOPACK POSTGRESQL STARTER

## PREREQUISITES

* NodeJS >= 8
* Docker

## START DEV ENVIRONMENT

```sh
docker-compose up
docker-compose exec node yarn database:migrate:up
```

Node is started in watch mode.

## STOP DEV ENVIRONMENT

```sh
docker-compose stop
```

## COMMANDS

Commands are done using `yarn` but you can use `npm`.

### RUN TESTS

```sh
yarn test
```

In watch mode (TDD)

```sh
yarn test:watch
```

## DATABASE MIGRATION

| operation          | command                                               |
| ------------------ | ----------------------------------------------------- |
| Create migration   | `yarn database:migrate:create -- <migration-name>`    |
| Apply migration    | `docker-compose exec node yarn database:migrate:up`   |
| Rollback migration | `docker-compose exec node yarn database:migrate:down` |

## HOW TO

### CONNECT TO POSTGRESQL DB

```sh
docker-compose exec postgresql sh
psql -U {DB_USER} {DB_NAME}
```
